//
//  WeatherData.swift
//  WeatherApp
//
//  Created by Alina Masetich on 4/30/19.
//  Copyright © 2019 Alina Masetich. All rights reserved.
//

import Foundation

class WeatherData {
    var temperature: Int
    var condition: Int
    var humidity: Int
    var city: String
    var weatherIconName: String

    init() {
        self.temperature = 0
        self.condition = 0
        self.humidity = 0
        self.city = ""
        self.weatherIconName = ""
    }
    
    func determineIcon(condition: Int) -> String {
        switch (condition) {
        case 0...300 :
            return "tstorm1"
        case 301...500 :
            return "light_rain"
        case 501...600 :
            return "shower3"
        case 601...700 :
            return "snow4"
        case 701...771 :
            return "fog"
        case 772...799 :
            return "tstorm3"
        case 800 :
            return "sunny"
        case 801...804 :
            return "cloudy2"
        case 900...903, 905...1000  :
            return "tstorm3"
        case 903 :
            return "snow5"
        case 904 :
            return "sunny"
        default :
            return "cloudy2"
        }
    }
}
