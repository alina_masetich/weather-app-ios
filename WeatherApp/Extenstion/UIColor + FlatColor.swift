//
//  UIColor + FlatColor.swift
//  WeatherApp
//
//  Created by Alina Masetich on 4/30/19.
//  Copyright © 2019 Alina Masetich. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    struct FlatColor {
        struct Purple {
            static let light = UIColor(netHex: 0x9C27B0)
        }
        
        struct Pink {
            static let saturated = UIColor(netHex: 0xE91E63)
        }
        
        struct Yellow {
            static let gold = UIColor(netHex: 0xFF9800)
            static let information = UIColor(netHex: 0xFFC107)
        }
    }
}

