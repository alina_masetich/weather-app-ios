//
//  ColorExtenstion.swift
//  WeatherApp
//
//  Created by Alina Masetich on 4/30/19.
//  Copyright © 2019 Alina Masetich. All rights reserved.
//

import UIKit

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        let invalidComponent = "Invalid component"
        assert(red >= 0 && red <= 255, invalidComponent)
        assert(green >= 0 && green <= 255, invalidComponent)
        assert(blue >= 0 && blue <= 255, invalidComponent)
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(netHex: Int) {
        self.init(red:(netHex >> 16) & 0xff, green: (netHex >> 8) & 0xff, blue: netHex & 0xff)
    }
}
