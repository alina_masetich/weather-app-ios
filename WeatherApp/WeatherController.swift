//
//  ViewController.swift
//  WeatherApp
//
//  Created by Alina Masetich on 4/25/19.
//  Copyright © 2019 Alina Masetich. All rights reserved.
//

import UIKit
import Alamofire
import CoreLocation
import SwiftyJSON

class WeatherController: UIViewController {

    @IBOutlet weak var weatherDetailsView: UIView!
    @IBOutlet weak var inputCityTextField: UITextField!
    @IBOutlet weak var cityView: UIView!
    @IBOutlet weak var temperatureView: UIView!
    @IBOutlet weak var humidityView: UIView!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var weatherIcon: UIImageView!
    @IBOutlet weak var cityLabel: UILabel!
    
    private let locationManager = CLLocationManager()
    private let WEATHER_URL = "http://api.openweathermap.org/data/2.5/weather"
    private let APP_ID = "e72ca729af228beabd5d20e3b7749713"
    private let weatherData = WeatherData()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLocationManager()
        setupView()
    }
    
    private func setupView() {
        weatherDetailsView.layer.cornerRadius = 15
        humidityView.layer.cornerRadius = 15
        humidityView.layer.maskedCorners = [.layerMaxXMaxYCorner]
        temperatureView.layer.cornerRadius = 15
        temperatureView.layer.maskedCorners = [.layerMinXMaxYCorner]
        searchButton.layer.cornerRadius = 5
    }
    
    private func setupLocationManager() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    @IBAction func searchButton(_ sender: UIButton) {
        guard let city = inputCityTextField.text, !city.isEmpty else { return }
        let params = ["q" : city, "appid" : APP_ID]
        getWeather(parameters: params)
    }
    
    private func getWeather(parameters: [String: String]) {
        Alamofire.request(WEATHER_URL, method: .get, parameters: parameters).responseJSON {
            response in
            if response.result.isSuccess {
                let weatherJSON = JSON(response.result.value!)
                log.debug(weatherJSON)
                self.updateWeather(json: weatherJSON)
            } else {
                self.alert(message: response.result.error?.localizedDescription ?? "")
            }
            self.view.endEditing(true)
        }
    }
    
    private func updateWeather(json : JSON) {
        let temp = json["main"]["temp"].doubleValue
        weatherData.temperature = Int(temp - 273.15)
        weatherData.humidity = Int(json["main"]["humidity"].intValue)
        weatherData.city = json["name"].stringValue
        weatherData.condition = json["weather"][0]["id"].intValue
        weatherData.weatherIconName = weatherData.determineIcon(condition: weatherData.condition)
        cityLabel.text = weatherData.city
        temperatureLabel.text = "\(weatherData.temperature)°"
        humidityLabel.text = "\(weatherData.humidity)%"
        weatherIcon.image = UIImage(named: weatherData.weatherIconName)
    }
    
    private func alert(message: String ) {
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
}

extension WeatherController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[locations.count - 1]
        if location.horizontalAccuracy > 0 {
            self.locationManager.stopUpdatingLocation()
            let latitude = String(location.coordinate.latitude)
            let longitude = String(location.coordinate.longitude)
            log.debug("longitude = \(longitude), latitude = \(latitude)")
            let params = ["lat" : latitude, "lon" : longitude, "appid" : APP_ID]
            getWeather(parameters: params)
        }
    }
}

